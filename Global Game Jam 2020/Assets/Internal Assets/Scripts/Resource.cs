﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Resource : MonoBehaviour, IInteractable
{
    public InteractableUI InteractableUi;
    public GameObject ItemPrefab;

    public void OnTriggerEnter(Collider col)
    {
        if (col.tag == Constants.Tags.Player)
        {

            var player = col.gameObject.GetComponent<Player>();

            if (player.PlayerState.Equals(PlayerState.HoldingItem)) return;

            player.ActiveInteraction = this;
            InteractableUi.SetInteractionButton(col.gameObject.GetComponent<Player>());
        }
    }

    public void OnTriggerExit(Collider col)
    {
        if (col.tag == Constants.Tags.Player)
        {
            InteractableUi.ButtonRenderer.enabled = false;

            var player = col.gameObject.GetComponent<Player>();

            if (!player.PlayerState.Equals(PlayerState.HoldingItem))
            {
                player.ActiveInteraction = null;
            }
        }
    }

    public void Interact(Player player)
    {
        var item = Instantiate(ItemPrefab, transform.position, transform.rotation).GetComponent<Item>();
        item.Interact(player);
        player.ActiveInteraction = item;

        InteractableUi.ButtonRenderer.enabled = false;
    }
}
