﻿using System;
using System.Collections.Generic;
using DG.Tweening;
using UniRx;
using UnityEngine;
using UnityEngine.UI;

public class Ticket : MonoBehaviour, IInteractable
{
    public InteractableUI InteractableUi;
    public float ActionTime = 3f;
    public bool FinishWithoutAction = false;
    public bool DestroyOnDeath = false;
    public bool IsInitiated = false;

    [Header("UI")]
    public SpritesData Data;
    //public Sprite PlankIcon;
    //public Sprite MetalIcon;
    //public Sprite CannonballIcon;
    //public Sprite CircleOff;
    //public Sprite CircleOn;
    public List<TicketUiItem> TicketUis;

    public GameObject ProgressBar;
    public Image ProgressFill;

    [HideInInspector] public TicketData TicketData;
    [HideInInspector] public Player CurrentPlayer;
    public Tweener FillTween;
    public GameObject FloorPart;

    [Header("Cannon")]
    public ParticleSystem ShootEffect;
    public ParticleSystem FireEffect;

    private IDisposable TicketTimerDisposable;
    private int TicketTimer = 0;

    public void Interact(Player player)
    {
        if (player.PlayerState.Equals(PlayerState.HoldingItem) && TicketData != null)
        {
            TicketUis.Find(ticketUI => ticketUI.ItemType.Equals(player.HoldingItemType) && !ticketUI.IsReady)
                .SetAsReady(Data.CircleOn);
            CheckIfTicketReadyToAction(player);
            player.RemoveHoldingItem();
        }
        else if (AllTicketItemsReady())
        {
            StartTicket(player);
        }
    }

    public void InitiateTicket(TicketData data)
    {
        TicketData = data;
        ResetTicketUis();
        IsInitiated = true;
        Container.Get<GameController>().ChangeShipHealth(-data.StartDamage);
        
        for (int i = 0; i < data.TicketItems.Count; i++)
        {
            TicketUis[i].Root.SetActive(true);
            TicketUis[i].ItemType = data.TicketItems[i];

            switch (data.TicketItems[i])
            {
                case ItemType.Plank:
                {
                    TicketUis[i].Icon.sprite = Data.PlankIcon;
                    break;
                }
                case ItemType.Metal:
                {
                    TicketUis[i].Icon.sprite = Data.MetalIcon;
                    break;
                }
                case ItemType.Cannonball:
                {
                    TicketUis[i].Icon.sprite = Data.CannonballIcon;
                    break;
                }
            }
        }

        if (TicketData.Type == TicketType.Cannon)
        {
            FireEffect.Play(true);
        }

        TicketTimer = TicketData.FullTime;
        TicketTimerDisposable = Observable.Timer(TimeSpan.FromSeconds(1f))
            .RepeatSafe()
            .Subscribe(x =>
            {
                TicketTimer--;

                if (TicketData == null)
                {
                    TicketTimerDisposable?.Dispose();
                    return;
                }

                if (TicketTimer == TicketData.YellowTime)
                {
                    TicketUis.ForEach(ui => ui.IconBackground.color = Color.yellow);
                }
                else if (TicketTimer == TicketData.RedTime)
                {
                    TicketUis.ForEach(ui => ui.IconBackground.color = Color.red);
                }
                else if (TicketTimer == 0)
                {
                    Container.Get<GameController>().ChangeShipHealth(-TicketData.EndDamage);
                    OnTicketCompleted();
                    TicketTimerDisposable?.Dispose();
                }
            });
    }

    public void StartTicket(Player player)
    {
        CurrentPlayer = player;
        player.PlayerState = PlayerState.DoingAction;
        player.PlayerRigidbody.velocity = Vector3.zero;
        ProgressBar.SetActive(true);
        ProgressFill.gameObject.SetActive(true);
        TicketUis.ForEach(tui => tui.Root.SetActive(false));
        InteractableUi.ButtonRenderer.enabled = false;

        ProgressFill.fillAmount = 0f;
        FillTween = ProgressFill.DOFillAmount(1f, ActionTime)
            .SetEase(Ease.Linear)
            .OnComplete(() =>
            {
                OnTicketCompleted();
            });
    }

    public void PauseTicket(Player player)
    {
        //player.PlayerState = PlayerState.None;
    }

    public void OnTicketCompleted()
    {
        Debug.Log("Ticket Completed");

        Container.Get<GameController>().ChangeShipHealth((float)TicketTimer / TicketData.FullTime * TicketData.StartDamage);

        if (FireEffect != null)
        {
            FireEffect.Stop(true);
        }

        IsInitiated = false;
        TicketData = null;
        if (CurrentPlayer != null)
        {
            CurrentPlayer.PlayerState = PlayerState.None;
        }

        if (TicketTimer != 0 && ShootEffect != null)
        {
            ShootEffect.Play(true);
        }
        
        ProgressBar.SetActive(false);
        ProgressFill.gameObject.SetActive(false);
        ResetTicketUis();

        if (FloorPart != null)
        {
            FloorPart.SetActive(true);
        }

        if (DestroyOnDeath)
        {
            Destroy(gameObject);
        }
    }

    public void ResetTicketUis()
    {
        foreach (var ticketUi in TicketUis)
        {
            ticketUi.Root.SetActive(false);
            ticketUi.IconBackground.color = Color.white;
            ticketUi.IsReady = false;
            ticketUi.Tick.sprite = Data.CircleOff;
        }

        TicketUis[3].IsReady = true;
        TicketUis[3].Tick.gameObject.SetActive(false);
    }

    public void OnTriggerEnter(Collider col)
    {
        if (col.tag == Constants.Tags.Player)
        {
            var player = col.gameObject.GetComponent<Player>();

            if (player.PlayerState.Equals(PlayerState.HoldingItem) && IsTicketNeedThisItem(player.HoldingItemType))
            {
                InteractableUi.Type = InteractionType.Resource;
                InteractableUi.SetInteractionButton(player);
                player.ActiveInteraction = this;
            }
            else if (AllTicketItemsReady())
            {
                InteractableUi.Type = InteractionType.Action;
                InteractableUi.SetInteractionButton(player);
                player.ActiveInteraction = this;
            }
        }
    }

    public void OnTriggerExit(Collider col)
    {
        if (col.tag == Constants.Tags.Player)
        {
            InteractableUi.ButtonRenderer.enabled = false;
            var player = col.gameObject.GetComponent<Player>();
            player.ActiveInteraction = null;
        }
    }

    public bool IsTicketNeedThisItem(ItemType type)
    {
        return TicketUis.Exists(ticketItem => 
            ticketItem.Root.activeSelf && !ticketItem.IsReady && ticketItem.ItemType.Equals(type));
    }

    public bool AllTicketItemsReady()
    {
        return TicketUis.FindAll(t => t.Root.activeSelf).Count > 0
        && TicketUis.FindAll(t => t.Root.activeSelf).TrueForAll(ticket => ticket.IsReady);
    }

    public void CheckIfTicketReadyToAction(Player player)
    {
        if (AllTicketItemsReady())
        {
            TicketUis[3].Root.SetActive(true);

            if (FinishWithoutAction)
            {
                TicketUis.ForEach(tui => tui.Root.SetActive(false));
                CurrentPlayer = player;
                OnTicketCompleted();
                return;
            }

            InteractableUi.Type = InteractionType.Action;
            InteractableUi.SetInteractionButton(player);
            player.ActiveInteraction = this;
        }
    }
}

[Serializable]
public class TicketUiItem
{
    public ItemType ItemType;
    public GameObject Root;
    public Image IconBackground;
    public Image Icon;
    public Image Tick;
    public bool IsReady;

    public void SetAsReady(Sprite circleOff)
    {
        IsReady = true;
        Tick.sprite = circleOff;

        Observable.Timer(TimeSpan.FromSeconds(0.5f))
            .Subscribe(x =>
            {
                Root.SetActive(false);
            });
    }
}
