﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using DG.Tweening;
using UnityEngine;

public class Item : MonoBehaviour, IInteractable
{
    public ItemType Type;
    public InteractableUI InteractableUi;
    public Rigidbody Rigidbody;
    public bool IsPlayerHoldingMe = false;
    public Player HoldingPlayer;
    public GameObject ReviveLock;
    //public bool IsInteractable = true;

    //public void OnEnable()
    //{
    //    ReviveLock.SetActive(true);
    //}

    public void OnTriggerEnter(Collider col)
    {
        if (col.tag == Constants.Tags.Player)
        {
            var player = col.gameObject.GetComponent<Player>();

            if (player.PlayerState.Equals(PlayerState.HoldingItem) || player.IsDead) return;

            InteractableUi.SetInteractionButton(player);
            player.ActiveInteraction = this;
        }
    }

    public void OnTriggerExit(Collider col)
    {
        if (col.tag == Constants.Tags.Player)
        {
            InteractableUi.ButtonRenderer.enabled = false;

            if (!IsPlayerHoldingMe)
            {
                var player = col.gameObject.GetComponent<Player>();
                player.ActiveInteraction = null;
            }
        }
    }

    public void Interact(Player player)
    {
        if (Type.Equals(ItemType.Revive))
        {
            var deadPlayer = FindObjectsOfType<Player>().ToList().Find(p => p.IsDead);

            if (deadPlayer == null) return;

            deadPlayer.Revive();
            InteractableUi.ButtonRenderer.enabled = false;
            gameObject.SetActive(false);
            player.PlayersAnimator.SetTrigger(Constants.Anims.Pickup);
        }
        else if (HoldingPlayer != null && HoldingPlayer != player)
        {
            HoldingPlayer.PlayerState = PlayerState.None;
            HoldingPlayer.ActiveInteraction = null;
            HoldingPlayer = player;

            player.ActiveInteraction = this;
            player.PlayerState = PlayerState.HoldingItem;
            transform.SetParent(player.gameObject.transform);
            transform.localPosition = new Vector3(0f, 0f, 0.5f);
            transform.DOLocalMoveY(1f, 0.25f).SetDelay(0.2f);
            player.PlayersAnimator.SetTrigger(Constants.Anims.Pickup);
        }
        else if (IsPlayerHoldingMe)
        {
            IsPlayerHoldingMe = false;
            player.PlayerState = PlayerState.None;
            HoldingPlayer = null;

            Rigidbody.isKinematic = false;
            transform.SetParent(null);
            player.PlayersAnimator.SetTrigger(Constants.Anims.Pickup);
        }
        else
        {
            IsPlayerHoldingMe = true;
            HoldingPlayer = player;
            player.PlayerState = PlayerState.HoldingItem;
            player.ActiveInteraction = null;

            Rigidbody.isKinematic = true;
            transform.SetParent(player.gameObject.transform);
            transform.localPosition = new Vector3(0f, 0f, 0.5f);
            transform.DOLocalMoveY(1f, 0.25f).SetDelay(0.2f);
            player.PlayersAnimator.SetTrigger(Constants.Anims.Pickup);
        }
    }
}

public enum ItemType
{
    Plank,
    Metal,
    Cannonball,
    Revive
}
