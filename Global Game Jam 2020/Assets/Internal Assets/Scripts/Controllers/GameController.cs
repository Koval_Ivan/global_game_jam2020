﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using DG.Tweening;
using UniRx;
using UnityEngine;
using UnityEngine.UI;
using Random = System.Random;

public class GameController : MonoBehaviour, IController
{
    public int ShipHeight = 10;
    public int ShipWidth = 18;

    public float TopLeftPosX = -8f;
    public float TopLeftPosY = 4.5f;
    public float Step = 1f;

    public GameObject PlayerPrefab;
    public GameObject ReviveItem;

    public List<PositionData> ObjectsOnShip;
    public List<List<GameObject>> Deck;

    public TicketConfig TicketConfig;
    public List<Ticket> CannonTickets;
    public List<Ticket> CannonballsTickets;
    public GameObject HoleTicketPrefab;

    [Header("Hole anim")]
    public GameObject Core;
    public GameObject DamageCircles;
    public GameObject ExplosionEffect;
    public float CoreFallTime = 4f;
    public Ease CoreFallEase = Ease.InCirc;

    public Text TimerText;
    public int Time = 0;
    private IDisposable TimerDisposable;

    [Header("HP")]
    public float ShipHealth = 100f;
    public float ShipMaxHealth = 100f;
    public Image HPFill;

    public List<GamepadData> ActivePlayers;

    [Header("UI")]
    public int LevelTime = 120;
    public GameObject EndGame;
    public Text EndGameText;

    private void OnDestroy()
    {
        Container.Remove(ControllerType);
    }

    public Type ControllerType => typeof(GameController);

    public void Initialize()
    {
        Container.Get<AudioController>().PlayLoopSound(SoundName.MainMenuTheme);

        foreach (var joystickName in Input.GetJoystickNames())
        {
            Debug.Log(joystickName);
        }

        UnityEngine.Time.timeScale = 1f;
        ObjectsOnShip = new List<PositionData>();
        Deck = new List<List<GameObject>>();
        var allObjects = FindObjectsOfType<PositionData>();

        foreach (var gamePlayObject in allObjects)
        {
            ObjectsOnShip.Add(gamePlayObject);
        }

        StartTimer();
        HPFill.fillAmount = 1f;
        Time = LevelTime;
        TimerText.text = LevelTime.ToString();

        ActivePlayers = new List<GamepadData>();
        StartCoroutine(ShootRoutine());

        //foreach (var joystickName in Input.GetJoystickNames())
        //{
        //    if (joystickName == "")
        //    {
        //        continue;
        //    }

        //    var player = Instantiate(PlayerPrefab, new Vector3(0f, 0f, -1f), Quaternion.Euler(0f, 90f, -90f));
        //    ActivePlayers.Add(new GamepadData(joystickName, player.GetComponent<Player>()));
        //}

        //Observable.Timer(TimeSpan.FromSeconds(0.5f))
        //    .RepeatSafe()
        //    .Subscribe(x =>
        //    {
        //        Debug.Log($"js {Input.GetJoystickNames().Length}");

        //        var namesWithoutSpaces = Input.GetJoystickNames().ToList().FindAll(n => n != "");

        //        if (namesWithoutSpaces.Count > ActivePlayers.Count)
        //        {
        //            var player = Instantiate(PlayerPrefab, new Vector3(0f, 0f, -1f), Quaternion.Euler(0f, 90f, -90f));
        //            ActivePlayers.Add(new GamepadData(namesWithoutSpaces.Last(), player.GetComponent<Player>()));
        //        }
        //        else if (namesWithoutSpaces.Count < ActivePlayers.Count)
        //        {
        //            foreach (var player in ActivePlayers)
        //            {
        //                if (!namesWithoutSpaces.Exists(p => p == player.GamepadName))
        //                {
        //                    Destroy(player.Player.gameObject);
        //                }
        //            }

        //            ActivePlayers.RemoveAll((p) => p.Player == null);
        //        }
        //    });

        //this.ObserveEveryValueChanged(x => Input.GetJoystickNames().Length)
        //    .Subscribe(x =>
        //    {

        //    });
    }

    public void Restart()
    {
        Container.Get<AudioController>().StopAllLoopSounds(true);
        UnityEngine.Time.timeScale = 1f;
        SceneLoader.LoadScene(SceneLoader.GetActiveSceneName());
    }

    private IEnumerator ShootRoutine()
    {
        while (true)
        {
            yield return new WaitForSeconds(10f);

            foreach (var cannonTicket in CannonTickets)
            {
                if (!cannonTicket.IsInitiated)
                {
                    cannonTicket.ShootEffect.Play(true);
                    yield return new WaitForSeconds(0.5f);
                }
            }
        }
    }

    public void StartTimer()
    {
        TimerDisposable = Observable.Timer(TimeSpan.FromSeconds(1f))
            .RepeatSafe()
            .Subscribe(x =>
            {
                Time--;

                if (Time == 0)
                {
                    GameOver(true);
                }

                TimerText.text = Time.ToString();

                var ticket = TicketConfig.Config.Find(t => t.AppearTime == (LevelTime - Time));

                if (ticket != null)
                {
                    ExecuteTicket(ticket);
                }
            })
            .AddTo(this);
    }

    public void GameOver(bool win)
    {
        if (win)
        {
            UnityEngine.Time.timeScale = 0f;
            EndGame.SetActive(true);
            EndGameText.text = "You win!";
            EndGameText.color = Color.green;
        }
        else
        {
            UnityEngine.Time.timeScale = 0f;
            EndGame.SetActive(true);
            EndGameText.text = "You lose!";
            EndGameText.color = Color.red;
        }
    }

    public void StopTimer()
    {
        TimerDisposable?.Dispose();
    }

    public void ExecuteTicket(TicketData ticket)
    {
        switch (ticket.Type)
        {
            case TicketType.Hole:
            {
                var randomPos = new Vector2Int(0, 0);
                //randomPos.x = UnityEngine.Random.Range(0, ShipWidth);
                //randomPos.y = UnityEngine.Random.Range(0, ShipHeight);
                randomPos.x = ticket.Pos.x;
                randomPos.y = ticket.Pos.y;

                var actualPos = new Vector3();
                actualPos.x = TopLeftPosX + (Step * randomPos.x);
                actualPos.y = TopLeftPosY - (Step * randomPos.y);
                actualPos.z = -1;

                //Debug.Log($"PosX: {randomPos.x}, PosY: {randomPos.y}, PosXF: {actualPos.x}, PosYF: {actualPos.y}");

                var core = Instantiate(Core, actualPos + new Vector3(0f, 0f, -19), Quaternion.identity);
                var damageCircles = Instantiate(DamageCircles, actualPos /*+ new Vector3(0f, 0f,f)*/,
                    Quaternion.identity);

                var explosionEffect = Instantiate(ExplosionEffect, actualPos, Quaternion.Euler(-90f, 0f, 0f));

                core.transform.DOMove(actualPos, CoreFallTime)
                    .SetEase(CoreFallEase)
                    .OnComplete(() =>
                    {
                        var hole = Instantiate(HoleTicketPrefab, actualPos, Quaternion.identity);
                        hole.GetComponent<Ticket>().InitiateTicket(ticket);

                        var floorPart = ObjectsOnShip.Find(obj => obj.Pos.x == randomPos.x && obj.Pos.y == randomPos.y
                                                                                           && obj.name.Contains("Box"));

                        hole.GetComponent<Ticket>().FloorPart = floorPart.gameObject;
                        floorPart.gameObject.SetActive(false);
                        explosionEffect.GetComponent<ParticleSystem>().Play(true);

                        foreach (var pl in damageCircles.GetComponent<HoleCircles>().PlayersInDanger)
                        {
                            pl.SetPlayerDead();
                        }

                        Destroy(core);
                        Destroy(damageCircles);
                    });

                break;
            }
            case TicketType.Cannon:
            {
                var notInitiatedCannons = CannonTickets.FindAll(t => !t.IsInitiated);
                notInitiatedCannons[UnityEngine.Random.Range(0, notInitiatedCannons.Count)].InitiateTicket(ticket);
                break;
            }
            case TicketType.Cannonball:
            {
                var notInitiatedCannonsballs = CannonballsTickets.FindAll(t => !t.IsInitiated);
                notInitiatedCannonsballs[UnityEngine.Random.Range(0, notInitiatedCannonsballs.Count)].InitiateTicket(ticket);
                break;
            }
        }
    }

    public void ChangeShipHealth(float changeAmount)
    {
        ShipHealth = Mathf.Clamp(ShipHealth + changeAmount, 0f, ShipMaxHealth);
        HPFill.fillAmount = ShipHealth / ShipMaxHealth;

        if (Mathf.Approximately(ShipHealth, 0f))
        {
            GameOver(false);
        }
    }

    public void OnLevelLoad()
    {

    }

    public void EnableController()
    {

    }

    public void DisableController()
    {

    }
}

public class GamepadData
{
    public string GamepadName;
    public Player Player;

    public GamepadData(string gamepadName, Player player)
    {
        GamepadName = gamepadName;
        Player = player;
    }
}