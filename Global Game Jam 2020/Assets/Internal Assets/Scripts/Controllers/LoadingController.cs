﻿using System;
using System.Collections.Generic;
using DG.Tweening;
using UniRx;
using UnityEngine;
using UnityEngine.UI;
using Random = UnityEngine.Random;

public class LoadingController : MonoBehaviour, IController
{
    [SerializeField] private CanvasGroup _fadeCanvasGroup;
    [SerializeField] private int _fadeTransitionTime = 1000;

    [HideInInspector] public bool IsEnabled = false;


    public Type ControllerType => typeof(LoadingController);

    public void Initialize()
    {

    }

    public void OnLevelLoad()
    {

    }

    public void EnableController()
    {

    }

    public void DisableController()
    {

    }

    public void EnableFade(int delayMs = 0, Action onEnabled = null)
    {
        if (IsEnabled) return;

        IsEnabled = true;
        _fadeCanvasGroup.blocksRaycasts = true;

        _fadeCanvasGroup.DOFade(1f, _fadeTransitionTime / 1000f)
            .SetDelay(delayMs / 1000f)
            .OnComplete(() =>
            {
                Observable.Timer(TimeSpan.FromSeconds(1.5f))
                    .Subscribe(x => { onEnabled?.Invoke(); });
            });
    }

    public void DisableFade(int delayMs = 0, Action onDisabled = null)
    {
        if (!IsEnabled)
        {
            Observable.Timer(TimeSpan.FromSeconds(1f))
                .Subscribe(x =>
                {
                    _fadeCanvasGroup.blocksRaycasts = false;
                    onDisabled?.Invoke();
                });

            return;
        }

        _fadeCanvasGroup.DOFade(0f, _fadeTransitionTime / 1000f)
            .SetDelay(delayMs / 1000f)
            .OnComplete(() =>
            {
                _fadeCanvasGroup.blocksRaycasts = false;
                onDisabled?.Invoke();
                IsEnabled = false;
            });
    }
}
