﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UniRx;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ControllersContainer : MonoBehaviour
{
    public bool InitializeOnAwake = true;
    private static bool IsContainerInitialized = false;
    private static bool IsControllersContainerCreated = false;
    public List<GameObject> ControllersPrefabs;
    private List<string> _noInitializeScenes = new List<string> {""};

    private void Awake()
    {
        Screen.sleepTimeout = SleepTimeout.NeverSleep;
        DontDestroyOnLoad(gameObject);
           
        if (IsControllersContainerCreated)
        {
            Destroy(gameObject);
            return;
        }

        QualitySettings.vSyncCount = 0;
        Application.targetFrameRate = 30;

        IsControllersContainerCreated = true;
        SceneManager.sceneLoaded -= CallOnLevelLoad;
        SceneManager.sceneLoaded += CallOnLevelLoad;

        if (InitializeOnAwake)
        {
            InitializeContainer();
        }
    }

    public void InitializeContainer()
    {
        foreach (var controllersPrefab in ControllersPrefabs)
        {
            var controller = Instantiate(controllersPrefab, Vector3.zero, Quaternion.identity, transform);
            var iController = controller.GetComponent<IController>();
            Container.Add(iController.ControllerType, iController);
        }

        foreach (var controller in Container.Controllers)
        {
            controller.Value.Initialize();
            controller.Value.DisableController();
        }

        IsContainerInitialized = true;
        CallOnLevelLoad(SceneLoader.GetActiveScene(), LoadSceneMode.Single);
    }

    public void EnableControllers()
    {
        foreach (var controller in Container.Controllers)
        {
            controller.Value.EnableController();
        }
    }

    public void DisableControllers()
    {
        foreach (var controller in Container.Controllers)
        {
            controller.Value.DisableController();
        }
    }

    public void CallOnLevelLoad(Scene scene, LoadSceneMode mode)
    {
        if (!IsContainerInitialized)
        {
            if (_noInitializeScenes.Contains(scene.name))
            {
                //Debug.Log($"Scene {scene.name} has been loaded. Scene is in black list for initializing.");
                return;
            }
            else
            {
                //Debug.Log($"Scene {scene.name} has been loaded. Initializing container.");
                InitializeContainer();
            }
        }
        else
        {
            if (_noInitializeScenes.Contains(scene.name))
            {
                DisableControllers();
                return;
            }

            var sceneControllers = FindObjectsOfType<MonoBehaviour>().OfType<IController>()
                .ToList().FindAll(controller => !Container.Contains(controller.ControllerType));

            foreach (var controller in sceneControllers)
            {
                Container.Controllers.Add(controller.ControllerType, controller);
            }

            foreach (var controller in sceneControllers)
            {
                controller.Initialize();
            }
        }

        //Debug.Log($"\"{scene.name}\" scene has been loaded. Container initialized, OnLevelLoad is called.");

        EnableControllers();

        foreach (var controller in Container.Controllers)
        {
            controller.Value.OnLevelLoad();
        }

        Container.Get<LoadingController>().DisableFade(500);
    }
}
