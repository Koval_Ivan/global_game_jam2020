﻿using System;
using System.Collections.Generic;
using DG.Tweening;
using UniRx;
using UnityEngine;

[RequireComponent(typeof(AudioSource)), RequireComponent(typeof(AudioListener))]
public class AudioController : MonoBehaviour, IController
{
    [SerializeField] private List<Sound> _sounds;

    public bool IsSoundsOn = true;
    public bool IsMusicOn = true;

    private AudioSource _mainAudioSource;
    private List<LoopSound> _loopSounds = new List<LoopSound>();

    private List<PlayedSound> _playedSounds = new List<PlayedSound>();
    private const float PlayThreshold = 0.05f;

    private List<SoundName> _backgroundSounds;

    public void MuteSounds()
    {
        _mainAudioSource.mute = true;
    }

    public void UnMuteSounds()
    {
        _mainAudioSource.mute = false;
    }

    public void SetMutedLoop(bool value)
    {
        foreach(var sound in _loopSounds)
        {
            sound.AudioSource.mute = value;
        }
    }

    public void PlaySound(SoundName soundName)
    {
        if (!IsSoundExists(soundName))
            return;

        //Check if given sound was not played with last [PlayThreshold] seconds. If yes - not playing it again
        var playedSound = _playedSounds.Find(sound => sound.Name == soundName);
        if (playedSound != null && Time.time - playedSound.LastPlayedTime < PlayThreshold)
            return;

        if (playedSound == null)
            _playedSounds.Add(new PlayedSound(soundName, Time.time));
        else
        {
            playedSound.LastPlayedTime = Time.time;
        }

        _mainAudioSource.PlayOneShot(GetAudioClip(soundName), GetVolume(soundName));
       
        if (!IsSoundsOn)
        {
            _mainAudioSource.mute = true;
        }
    }

    public void PlaySound(SoundName soundName, float delay)
    {
        Observable.Timer(TimeSpan.FromSeconds(delay))
            .Subscribe(x => { PlaySound(soundName); });
    }

    public void PlayLoopSound(SoundName soundName)
    {
        if (!IsSoundExists(soundName) || _loopSounds != null && _loopSounds.Exists(sound => sound.Name == soundName))
            return;
        if (_loopSounds == null)
            _loopSounds = new List<LoopSound>();

        var newLoopSource = gameObject.AddComponent<AudioSource>();
        newLoopSource.clip = GetAudioClip(soundName);
        newLoopSource.loop = true;
        newLoopSource.volume = 0f;
        newLoopSource.Play();
        newLoopSource.DOFade(GetVolume(soundName), 0.5f);

        _loopSounds.Add(new LoopSound(newLoopSource, soundName));
        if((soundName == SoundName.SandEffect && !IsSoundsOn) 
           || (soundName != SoundName.SandEffect && !IsMusicOn)) //Kostil bcoz someone broke my audio controller with this if >:(((
        {
            newLoopSource.mute = true;
        }
    }

    public void StopLoopSound(SoundName soundName)
    {
        if (!IsSoundExists(soundName) || _loopSounds == null)
            return;

        if (_loopSounds.Exists(sound => sound.Name == soundName))
        {
            var loopedSound = _loopSounds.Find(sound => sound.Name == soundName);
            loopedSound.AudioSource.DOFade(0f, 0.5f)
                .OnComplete(() =>
                {
                    loopedSound.AudioSource.Stop();
                    Destroy(loopedSound.AudioSource);
                    _loopSounds.Remove(loopedSound);
                });
        }
    }

    public void StopAllLoopSounds(bool includingMainTheme = false)
    {
        if (_loopSounds == null || _loopSounds.Count == 0)
            return;

        for (int i = 0; i < _loopSounds.Count; i++)
        {
            if (IsBackgroundMusic(_loopSounds[i].Name) && !includingMainTheme)
            {
                continue;
            }

            StopLoopSound(_loopSounds[i].Name);
        }
    }

    private AudioClip GetAudioClip(SoundName soundName)
    {
        return _sounds.Find(sound => sound.Name == soundName).Clip;
    }

    private float GetVolume(SoundName soundName)
    {
        return _sounds.Find(sound => sound.Name == soundName).Volume;
    }

    private bool IsSoundExists(SoundName soundName)
    {
        if (_sounds.Exists(x => x.Name == soundName))
            return true;

        Debug.LogError($"Sound {soundName} was not added to Audio Controller.");
        return false;
    }

    private bool IsBackgroundMusic(SoundName soundName)
    {
        return _backgroundSounds.Contains(soundName);
    }

    public Type ControllerType => typeof(AudioController);

    public void Initialize()
    {
        _mainAudioSource = GetComponent<AudioSource>();

        IsSoundsOn = PlayerPrefs.GetInt("Sounds Settings", 1) == 1;
        IsMusicOn = PlayerPrefs.GetInt("Music Settings", 1) == 1;

        _backgroundSounds = new List<SoundName>
        {
            SoundName.MainMenuTheme,
            SoundName.EvenLevelTheme,
            SoundName.OddLevelTheme
        };
    }

    public void OnLevelLoad()
    {

    }

    public void EnableController()
    {

    }

    public void DisableController()
    {

    }

    [Serializable]
    internal class Sound
    {
        public SoundName Name;
        public AudioClip Clip;

        [Range(0f, 1f)]
        public float Volume = 1f;
    }

    [Serializable]
    internal class LoopSound
    {
        public AudioSource AudioSource;
        public SoundName Name;

        public LoopSound(AudioSource audioSource, SoundName name)
        {
            AudioSource = audioSource;
            Name = name;
        }
    }

    internal class PlayedSound
    {
        public SoundName Name;
        public float LastPlayedTime;

        public PlayedSound(SoundName name, float lastPlayedTime)
        {
            Name = name;
            LastPlayedTime = lastPlayedTime;
        }
    }
}

public enum SoundName
{
    None,
    MainMenuTheme,
    AmazingWork,
    FireworksFanfare,
    Star1,
    Star2,
    Star3,
    EvenLevelTheme,
    OddLevelTheme,
    TileFlip,

    MenuScreenChange,
    ButtonBubble,
    ButtonGling,
    BombHit,
    LevelFailed,
    LockOpen,
    LockGreenParticles,
    BottleBreaks,
    BottleRowExplode,
    BoosterUsed,
    SandEffect,
    ChestOpen,

    MainMenuStar1,
    MainMenuStar2,
    MainMenuStar3,

    CoinsCollect,
    HeartsCollectStart,
    HeartsCollectEnd,
    BoostersCollectStart,
    BoostersCollectEnd,

    DailyBonus,
    DailyChallenge,
    SheepCollect,
    PuppyCollect,
    KittyCollect,
    PreGamePopupShow,
    PreGamePopupHide,
    RaceShift,
    ChestCardChoose,
    ChestCardHide
}
