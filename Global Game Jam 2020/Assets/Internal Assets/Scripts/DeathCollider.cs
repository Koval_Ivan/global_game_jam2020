﻿using UnityEngine;

public class DeathCollider : MonoBehaviour
{
    public GameObject ReviveItem;

    private void OnTriggerEnter(Collider col)
    {
        if (col.tag.Equals(Constants.Tags.Player))
        {
            col.GetComponent<Player>().SetPlayerDead();
        }
    }
}
