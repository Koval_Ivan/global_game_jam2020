﻿using UnityEngine;
using UnityEngine.UI;

public class InteractableUI : MonoBehaviour
{
    public InteractionType Type;

    public Image ButtonRenderer;
    public Sprite ButtonX;
    public Sprite ButtonY;
    public Sprite ButtonA;
    public Sprite ButtonB;

    private void Start()
    {
        ButtonRenderer.enabled = false;
    }

    public void SetInteractionButton(Player player)
    {
        //ButtonRenderer.sprite = Type.Equals(InteractionType.Resource) ? ButtonA : ButtonB;
        //ButtonRenderer.enabled = true;
    }
}
