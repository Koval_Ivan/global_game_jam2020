﻿using System;
using System.Text.RegularExpressions;
using UnityEngine;
using Object = UnityEngine.Object;

public static class Utilites
{
    public static float GetTime(float distance, float speed)
    {
        return distance / speed;
    }

    public static float GetTime(Vector2 from, Vector2 to, float speed)
    {
        return Vector2.Distance(from, to) / speed;
    }

    public static string SplitCamelCase(string str)
    {
        return Regex.Replace(Regex.Replace(str, @"(\P{Ll})(\P{Ll}\p{Ll})", "$1 $2"),
            @"(\p{Ll})(\P{Ll})", "$1 $2");
    }

    public static float ConvertRange(float originalStart, float originalEnd, float newStart, float newEnd, float value)
    {
        float scale = (newEnd - newStart) / (originalEnd - originalStart);
        return newStart + (value - originalStart) * scale;
    }

    public static float GetScreenCoefficient(bool lessThanZero)
    {
        return lessThanZero ? ((float)Screen.width / Screen.height) : ((float)Screen.height / Screen.width);
    }
}

public static class TypeExtensions
{
    public static bool IsSubclassOfRawGeneric(Type generic, Type toCheck)
    {
        while (toCheck != null && toCheck != typeof(object))
        {
            var cur = toCheck.IsGenericType ? toCheck.GetGenericTypeDefinition() : toCheck;
            if (generic == cur)
            {
                return true;
            }
            toCheck = toCheck.BaseType;
        }
        return false;
    }
}

public static class RectTransformExtensions
{
    public static bool IsVisibleWithinRect(this RectTransform thisRect, RectTransform rect, float padding = 0f)
    {
        Vector3[] thisRectBounds = new Vector3[4];
        thisRect.GetWorldCorners(thisRectBounds);

        float maxY = Mathf.Max(thisRectBounds[0].y, thisRectBounds[1].y, thisRectBounds[2].y, thisRectBounds[3].y);
        float minY = Mathf.Min(thisRectBounds[0].y, thisRectBounds[1].y, thisRectBounds[2].y, thisRectBounds[3].y);
        //No need to check horizontal visibility: there is only a vertical scroll rect
        float maxX = Mathf.Max(thisRectBounds[0].x, thisRectBounds[1].x, thisRectBounds[2].x, thisRectBounds[3].x);
        float minX = Mathf.Min(thisRectBounds[0].x, thisRectBounds[1].x, thisRectBounds[2].x, thisRectBounds[3].x);

        Vector3[] rectBounds = new Vector3[4];
        rect.GetComponent<RectTransform>().GetWorldCorners(rectBounds);

        float maxFieldY = Mathf.Max(rectBounds[0].y, rectBounds[1].y, rectBounds[2].y, rectBounds[3].y);
        float minFieldY = Mathf.Min(rectBounds[0].y, rectBounds[1].y, rectBounds[2].y, rectBounds[3].y);

        float maxFieldX = Mathf.Max(rectBounds[0].x, rectBounds[1].x, rectBounds[2].x, rectBounds[3].x);
        float minFieldX = Mathf.Min(rectBounds[0].x, rectBounds[1].x, rectBounds[2].x, rectBounds[3].x);

        return !(minY > maxFieldY - padding || maxY < minFieldY + padding)
            && !(minX > maxFieldX - padding || maxX < minFieldX + padding);
    }

    public static void SetLeft(this RectTransform rt, float left)
    {
        rt.offsetMin = new Vector2(left, rt.offsetMin.y);
    }

    public static void SetRight(this RectTransform rt, float right)
    {
        rt.offsetMax = new Vector2(-right, rt.offsetMax.y);
    }

    public static void SetTop(this RectTransform rt, float top)
    {
        rt.offsetMax = new Vector2(rt.offsetMax.x, -top);
    }

    public static void SetBottom(this RectTransform rt, float bottom)
    {
        rt.offsetMin = new Vector2(rt.offsetMin.x, bottom);
    }
}

public static class TransformExtensions
{
    public static void DestroyAllChildren(this Transform parent)
    {
        foreach (Transform child in parent)
        {
            Object.Destroy(child.gameObject);
        }
    }
}

public static class DateTimeExtensions
{
    public static DateTime Next(this DateTime from, DayOfWeek dayOfWeek)
    {
        int start = (int)from.DayOfWeek;
        int target = (int)dayOfWeek;
        if (target <= start)
            target += 7;
        return from.AddDays(target - start);
    }
}

public static class StringExtensions
{
    public static bool IsNullOrEmpty(this string str)
    {
        return string.IsNullOrEmpty(str);
    }
}
