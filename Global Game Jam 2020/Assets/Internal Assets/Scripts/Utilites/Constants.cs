﻿
public static class Constants
{
    internal static class Tags
    {
        public static string Player = "Player";
        public static string Resource = "Resource";
    }

    internal static class Anims
    {
        public static string Idle = "Idle";
        public static string IdleCarry = "Idle Carry";
        public static string Pickup = "Pickup";
        public static string Action = "Action";
        public static string Walk = "Walk";
        public static string WalkCarry = "Walk Carry";
    }
}
