﻿using UnityEngine.SceneManagement;

public static class SceneLoader
{
    public static Scene GetActiveScene()
    {
        return SceneManager.GetActiveScene();
    }

    public static string GetActiveSceneName()
    {
        return SceneManager.GetActiveScene().name;
    }

    public static void LoadScene(string sceneName, bool useFade = true)
    {
        if (useFade)
        {
            Container.Get<LoadingController>().EnableFade(0, () => { SceneManager.LoadScene(sceneName); });
        }
        else
        {
            SceneManager.LoadScene(sceneName);
        }
    }

    public static void LoadScene(int sceneBuildIndex)
    {
        Container.Get<LoadingController>().EnableFade(0, () => { SceneManager.LoadScene(sceneBuildIndex); });
    }

    public static void LoadSceneAsync(int sceneBuildIndex)
    {
        Container.Get<LoadingController>().EnableFade(0, () => { SceneManager.LoadScene(sceneBuildIndex); });
    }

    public static void LoadSceneAsync(string sceneName)
    {
        Container.Get<LoadingController>().EnableFade(0, () => { SceneManager.LoadScene(sceneName); });
    }
}
