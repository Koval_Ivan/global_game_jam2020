﻿using UnityEngine;

public static class Vibration
{

#if UNITY_ANDROID && !UNITY_EDITOR
    public static AndroidJavaClass UnityPlayer = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
    public static AndroidJavaObject CurrentActivity = UnityPlayer.GetStatic<AndroidJavaObject>("currentActivity");
    public static AndroidJavaObject Vibrator = CurrentActivity.Call<AndroidJavaObject>("getSystemService", "vibrator");
#else
    public static AndroidJavaClass UnityPlayer;
    public static AndroidJavaObject CurrentActivity;
    public static AndroidJavaObject Vibrator;
#endif

    /* !!!
     * In Order to this script work you must add the line below to manifest, as a direct child of the manifest element:
     * <uses-permission android:name="android.permission.VIBRATE" />
     */

    public static void Vibrate()
    {
#if UNITY_ANDROID && !UNITY_EDITOR
        Vibrator.Call("vibrate");
#endif
    }

    public static void Vibrate(long milliseconds)
    {
#if UNITY_ANDROID && !UNITY_EDITOR
        Vibrator.Call("vibrate", milliseconds);
#endif
    }

    public static void Vibrate(long[] pattern, int repeat)
    {
#if UNITY_ANDROID && !UNITY_EDITOR
        Vibrator.Call("vibrate", pattern, repeat);
#endif
    }

    public static void Cancel()
    {
#if UNITY_ANDROID && !UNITY_EDITOR
        Vibrator.Call("cancel");
#endif
    }
}
