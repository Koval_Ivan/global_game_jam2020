﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class Player : MonoBehaviour
{
    public string moveAxisHorizontal = "Horizontal";
    public string moveAxisVertical = "Vertical";
    public string InteractionButton = "Fire1";
    public float Speed = 2f;
    public float RotateSpeed = 2f;
    public Rigidbody PlayerRigidbody;
    public PlayerState PlayerState = PlayerState.None;
    public IInteractable ActiveInteraction;

    public Vector3 DeathPos = new Vector3(15f, 0f, -1f);
    public Vector3 RevivePos = new Vector3(13f, 0f, -1f);
    public bool IsDead = false;
    public Animator PlayersAnimator;

    public ItemType HoldingItemType => GetComponentInChildren<Item>().Type;

    private void Start()
    {

    }

    private void Update()
    {
        if (PlayerState.Equals(PlayerState.DoingAction) || IsDead) return;

        float horizontal = Input.GetAxis(moveAxisHorizontal) * Speed * Time.deltaTime;
        float vertical = Input.GetAxis(moveAxisVertical) * Speed * Time.deltaTime;

        //PlayerRigidbody.MovePosition(transform.position + new Vector3(horizontal, vertical));
        PlayerRigidbody.velocity = new Vector3(horizontal, vertical, PlayerRigidbody.velocity.z);

        if (!Mathf.Approximately(horizontal, 0f) || !Mathf.Approximately(vertical, 0f))
        {
            Rotate(horizontal, vertical);

            if (PlayerState.Equals(PlayerState.HoldingItem))
            {
                if (!PlayersAnimator.GetBool(Constants.Anims.WalkCarry))
                {
                    PlayersAnimator.SetBool(Constants.Anims.Walk, false);
                    PlayersAnimator.SetBool(Constants.Anims.WalkCarry, true);
                    PlayersAnimator.SetBool(Constants.Anims.Idle, false);
                    PlayersAnimator.SetBool(Constants.Anims.IdleCarry, false);
                }
            }
            else
            {
                if (!PlayersAnimator.GetBool(Constants.Anims.Walk))
                {
                    PlayersAnimator.SetBool(Constants.Anims.Walk, true);
                    PlayersAnimator.SetBool(Constants.Anims.WalkCarry, false);
                    PlayersAnimator.SetBool(Constants.Anims.Idle, false);
                    PlayersAnimator.SetBool(Constants.Anims.IdleCarry, false);
                }
            }
        }
        else
        {
            if (PlayerState.Equals(PlayerState.HoldingItem))
            {
                if (!PlayersAnimator.GetBool(Constants.Anims.IdleCarry))
                {
                    PlayersAnimator.SetBool(Constants.Anims.Walk, false);
                    PlayersAnimator.SetBool(Constants.Anims.WalkCarry, false);
                    PlayersAnimator.SetBool(Constants.Anims.Idle, false);
                    PlayersAnimator.SetBool(Constants.Anims.IdleCarry, true);
                }
            }
            else
            {
                if (!PlayersAnimator.GetBool(Constants.Anims.Idle))
                {
                    PlayersAnimator.SetBool(Constants.Anims.Walk, false);
                    PlayersAnimator.SetBool(Constants.Anims.WalkCarry, false);
                    PlayersAnimator.SetBool(Constants.Anims.Idle, true);
                    PlayersAnimator.SetBool(Constants.Anims.IdleCarry, false);
                }
            }
        }

        if (Input.GetButtonDown(InteractionButton))
        {
            Debug.Log($"GET DOWN {InteractionButton}");
            if (ActiveInteraction != null)
            {
                ActiveInteraction.Interact(this);
            }
            else if (PlayerState.Equals(PlayerState.HoldingItem))
            {
                GetComponentInChildren<Item>().Interact(this);
            }
        }
    }

    void Rotate(float h, float v)
    {
        Vector3 desiredDirection = Vector3.Normalize(new Vector3(h, v, 0f));
        if (desiredDirection != Vector3.zero)
        {
            Quaternion rotation = Quaternion.LookRotation(desiredDirection, Vector3.back);
            transform.rotation = Quaternion.Slerp(transform.rotation, rotation, RotateSpeed * Time.deltaTime);
        }
    }

    public void RemoveHoldingItem()
    {
        Destroy(GetComponentInChildren<Item>().gameObject);
        PlayerState = PlayerState.None;
    }

    public void SetPlayerDead()
    {
        Container.Get<GameController>().ReviveItem.SetActive(true);

        transform.position = DeathPos;
        IsDead = true;
        PlayerRigidbody.isKinematic = true;


        PlayersAnimator.SetBool(Constants.Anims.Walk, false);
        PlayersAnimator.SetBool(Constants.Anims.WalkCarry, false);
        PlayersAnimator.SetBool(Constants.Anims.Idle, true);
        PlayersAnimator.SetBool(Constants.Anims.IdleCarry, false);

        if (FindObjectsOfType<Player>().ToList().TrueForAll(pl => pl.IsDead))
        {
            Container.Get<GameController>().GameOver(false);
        }
    }

    public void Revive()
    {
        transform.position = RevivePos;
        IsDead = false;
        PlayerRigidbody.isKinematic = false;
    }

    public void OnTriggerEnter(Collider col)
    {

    }

    public void OnTriggerExit(Collider col)
    {

    }
}

public enum PlayerState
{
    None,
    DoingAction,
    HoldingItem
}
