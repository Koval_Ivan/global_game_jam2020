﻿using DG.Tweening;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    public float RotationDelta = 2f;
    public float Amplitude = 1f;
    public Ease AnimEase = Ease.OutQuad;

    void Start()
    {
        var maxRotation = transform.rotation.eulerAngles.x + RotationDelta;
        var minRotation = transform.rotation.eulerAngles.x - RotationDelta;

        transform.rotation = Quaternion.Euler(maxRotation, 0f, 0.5f);

        Sequence seq = DOTween.Sequence();
        seq.Append(transform.DORotate(new Vector3(minRotation, 0f, -0.5f), Amplitude).SetEase(AnimEase));
        seq.Append(transform.DORotate(new Vector3(maxRotation, 0f, -0.5f), Amplitude).SetEase(AnimEase));
        seq.Append(transform.DORotate(new Vector3(minRotation, 0f, 0.5f), Amplitude).SetEase(AnimEase));
        seq.Append(transform.DORotate(new Vector3(maxRotation, 0f, 0.5f), Amplitude).SetEase(AnimEase));
        seq.SetLoops(-1);
    }
}
