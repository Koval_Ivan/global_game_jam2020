﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HoleCircles : MonoBehaviour
{
    public List<Player> PlayersInDanger;

    private void Awake()
    {
        PlayersInDanger = new List<Player>();
    }

    private void OnTriggerEnter(Collider col)
    {
        if (col.tag.Equals(Constants.Tags.Player))
        {
            if (!PlayersInDanger.Contains(col.GetComponent<Player>()))
            {
                PlayersInDanger.Add(col.GetComponent<Player>());
            }
        }
    }

    private void OnTriggerExit(Collider col)
    {
        if (col.tag.Equals(Constants.Tags.Player))
        {
            if (PlayersInDanger.Contains(col.GetComponent<Player>()))
            {
                PlayersInDanger.Remove(col.GetComponent<Player>());
            }
        }
    }
}
