﻿using System;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class TicketData
{
    public int AppearTime;
    public TicketType Type;
    public List<ItemType> TicketItems;

    [Space]
    public int FullTime;
    public int YellowTime;
    public int RedTime;

    [Space]
    public float StartDamage;
    public float EndDamage;

    public Vector2Int Pos;
}

public enum TicketType
{
    Hole,
    Cannon,
    Cannonball
}
