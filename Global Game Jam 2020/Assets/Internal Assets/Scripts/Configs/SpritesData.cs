﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Ticket Sprites Data", menuName = "TSD")]
public class SpritesData : ScriptableObject
{
    public Sprite PlankIcon;
    public Sprite MetalIcon;
    public Sprite CannonballIcon;
    public Sprite RepairIcon;
    public Sprite LockIcon;
    public Sprite CircleOff;
    public Sprite CircleOn;
}
