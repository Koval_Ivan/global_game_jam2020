﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Ticket Config", menuName = "Ticket Config")]
public class TicketConfig : ScriptableObject
{
    public List<TicketData> Config;
}
